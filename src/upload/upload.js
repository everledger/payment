import React, {useState, useEffect} from "react";

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


import axios from 'axios';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
}));


function Upload() {

    const classes = useStyles();


    function handleFileInput(e)
    {

      console.log(e.target.files[0].name)

      let fd = new FormData();

      fd.append("file",e.target.files[0]);

      axios.post("http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/upload.jsp", fd, { // receive two parameter endpoint url ,form data 
      })
      .then(res => { // then print response status
        console.log(res)
      })

    }

    return  <center>
    <br/><br/><br/><br/>


    <br/><br/>
        <input
          className={classes.input}
          style={{ display: 'none' }}
          id="raised-button-file"
          multiple
          type="file"
          onChange={ (e) => handleFileInput(e) }
        />
        <label htmlFor="raised-button-file">
          <Button variant="contained" component="span" className={classes.button}>
            Upload
          </Button>
        </label> 
    </center>;

}

  export default Upload;
  