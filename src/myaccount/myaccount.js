import React, {useState, useEffect} from "react";
import { Link, Redirect } from "react-router-dom";
import { useGlobal} from 'reactn';

import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Modal from '@material-ui/core/Modal';

//import TextField from '@material-ui/core/TextField';
//import InputAdornment from '@material-ui/core/InputAdornment';

import useDataApi from "../use_data_api/usedataapi"
import axios from 'axios';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
  card: {
    margin: theme.spacing(1),
  },
}));


function Myaccount() {

    const classes = useStyles();

    const [ global, setGlobal ] = useGlobal();
    const [modalOpen, setModalOpen] = useState(false);
    const [modalURL, setModalURL] = useState("");

    const [{ data, isLoading, isError }, doFetch] = useDataApi(
      'http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler.jsp?callType=get_myaccount&user_id=' + global.user_id,
      {ledger:{amount:0,iban:""},
        user:{company:{name:"",registration_number:""}},
        transactions:[{t_id:0,t_id_to_escrow:"",img:"",t_amount:0,url:"",cert_id:"",t_status:0,time_to_escrow:"", buyer_name:"", seller_name:"",seller_id:0,invoice:""}]
      }
    );  


/*
    const [{ data, isLoading, isError }, doFetch] = useDataApi(
      'http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler.jsp?callType=get_myaccount&user_id=' + global.user_id,
      {ledger:{amount:0,iban:""},
        user:{company:{name:"",registration_number:""}},
        transactions:[{t_id_to_escrow:"",img:"",t_amount:0,url:"",cert_id:"",t_status:0}]
      }
    );  
*/
    const handleModalOpen = (url) => {
      setModalURL(url)
      setModalOpen(true);
    };
  
    const handleModalClose = () => {
      setModalOpen(false);
    };

    function handleFileInput(e,t_id)
    {

      let fileName = e.target.files[0].name

      let fd = new FormData();

      fd.append("file",e.target.files[0]);

      axios.post("http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/upload.jsp", fd, { // receive two parameter endpoint url ,form data 
      })
      .then(res => { // then print response status
        //console.log(res)
        doFetch(
          encodeURI('http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler.jsp?callType=complete_transaction&t_id=' + t_id + '&user_id=' + global.user_id + '&invoice=' + fileName),
        );
      })

      //setTimeout(reload(), 10000)

    }

    function reload(){
      //console.log("here")
      doFetch(
        encodeURI('http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/data_handler.jsp?callType=get_myaccount&user_id=' + global.user_id),
      );  
    }

    //useEffect(() => console.log(data.ledger.amount), [data.ledger.amount]);


    if (global.user_id>0){

      return  <Box>
          <Modal open={modalOpen} onClose={handleModalClose}>
            <div style={{width:"70%",height:"80%",left:"15%",top:"10%",position:"absolute"}}>
              <iframe src={modalURL} width="100%" height="100%"></iframe>
            </div>
          </Modal>
          <br/><br/><span className="pagetitle">My Account</span>
          <span style={{position:"absolute",right:"10%"}}>
            <Button component={Link} to="/assets" size="large" variant="outlined" className={classes.button}>
              Diamonds for sale&nbsp;&nbsp;
              <i className="material-icons">
              shopping_basket
              </i>
            </Button>
          </span>

          <br/><br/>
          <span className="row">
            <span className="col" onClick={reload}>
              <span className="subtitle"><Icon style={{position:"relative",top:"5px"}}>account_balance_wallet</Icon> Balance</span>
              <br/>
              {isLoading ? (
                <span><CircularProgress size="15px" /></span>
              ) : (
                <span>${data.ledger.amount}</span>
              )}
            </span>
            <span className="col">
              <span className="subtitle"><Icon style={{position:"relative",top:"5px"}}>business</Icon> Company</span>
              <br/>
              {isLoading ? (
                <span><CircularProgress size="15px" /></span>
              ) : (
                <span>{data.user.company.name}</span>
              )}
              {isLoading ? (
                <span><CircularProgress size="15px" /></span>
              ) : (
                <span>{data.user.company.registration_number}</span>
              )}
            </span>
            <span className="col">
              <span className="subtitle"><Icon style={{position:"relative",top:"5px"}}>account_balance</Icon> IBAN</span>
              <br/>
              {isLoading ? (
                <span><CircularProgress size="15px" /></span>
              ) : (
                <span>{data.ledger.iban}</span>
              )}
            </span>
          </span>
          <span>
            <span className="subtitle"><Icon style={{position:"relative",top:"5px"}}>import_export</Icon> Transactions:</span>
            {data.transactions.map((transaction, index)=>{
                return <Card key={index} className={classes.card}>
                <CardContent>
                  {transaction.t_status == 1 ? (
                    <span>Status: Money transferred to escrow</span>
                  ) : (
                    <span>Status: Completed</span>
                  )}
                  <br/>
                  <img src={transaction.img} width="50px" style={{float:"left",margin:"5px"}}></img>
                  <br/>
                  <span className="grey_it">{transaction.cert_id}</span>
                  <br/>
                  <span className="bold_it">
                    ${transaction.t_amount}
                  </span>
                  <br/><br/>
                  <span className="grey_it">
                    Transaction id to escrow ledger: {transaction.t_id_to_escrow} (Transfer time: {transaction.time_to_escrow} GMT)
                  </span>
                  <br/>
                  {transaction.t_id_from_escrow != null ? (
                    <span className="grey_it">
                    Transaction id from escrow ledger: {transaction.t_id_from_escrow} (Transfer time: {transaction.time_from_escrow} GMT)
                    <br/></span>
                  ) : (
                    <span></span>
                  )}
                  <span className="grey_it">
                    Buyer: {transaction.buyer_name}
                    <br/>
                    Seller: {transaction.seller_name}
                  </span>
                  
                </CardContent>
                <CardActions>
                  <Button variant="outlined" color="primary" onClick={()=>handleModalOpen(transaction.url)}>Provenance details</Button>
                  {transaction.t_status == 1 && transaction.seller_id == global.user_id ? (
                    <span>
                      <input
                      className={classes.input}
                      style={{ display: 'none' }}
                      id="raised-button-file"
                      multiple
                      type="file"
                      onChange={ (e) => handleFileInput(e,transaction.t_id) }
                      />
                      <label htmlFor="raised-button-file">
                        <Button variant="contained" color="primary" component="span" className={classes.button}>
                          Upload invoice
                        </Button>
                      </label> 
                    </span>          
                  ) : (
                    <span></span>
                  )}
                  {transaction.t_status == 2 ? (
                    <span>
                      <Button variant="outlined" color="primary" onClick={()=>handleModalOpen("http://ec2-35-176-106-184.eu-west-2.compute.amazonaws.com:8080/uploaded/" + transaction.invoice)}>Invoice</Button>
                    </span>          
                  ) : (
                    <span></span>
                  )}

                </CardActions>
              </Card>

            })}
          </span>
        </Box>;

    }else{

      return <Redirect to='/login' />

    }

}

  export default Myaccount;
  